import React from "react";
import classes from "./SideDrawer.module.css";
import Logo from "../Logo/Logo";
import NavigationItems from "../NavigationItems/NavigationItems";
import Backdrop from "../UI/Backdrop/Backdrop";
import Auxi from "../../hoc/_Aux/Auxi";

const sideDrawer = (props) => {
  let attachedClasses = [classes.SideDrawer, classes.Close];
  if (props.open) {
    attachedClasses = [classes.SideDrawer, classes.Open];
  }
  return (
    <Auxi>
      <Backdrop show={props.open} clicked={props.closed} />
      <div className={attachedClasses.join(' ')}>
        <Logo height="40%" backgroundColor="white" />
        <nav>
          <NavigationItems />
        </nav>
      </div>
    </Auxi>
  );
};

export default sideDrawer;
