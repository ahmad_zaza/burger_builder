import React, { useEffect, useState } from "react";
import classes from "./ContactData.module.css";
import { useNavigate, useOutletContext } from "react-router-dom";
import Spinner from "../../../Components/UI/Spinner/Spinner";
import axios from "../../../axios-order";
import Button from "../../../Components/UI/Button/Button";


export default function ContactData() {
  const [loading, setLoading] = useState(false);
  const [ingredients] = useOutletContext();
  const [price] = useOutletContext();
  const navigate = useNavigate();

  const orderHandler = (event) => {
    event.preventDefault();
    setLoading(true);
    const order = {
      ingredients: ingredients,
      price: price,
      customer: {
        name: "Ahmad Zaza",
        address: {
          street: "Teststreet 1",
          zipCode: "41351",
          country: "Syria",
        },
        email: "ahmadzazaz98@gmail.com",
      },
      deliveryMethod: "fastest",
    };
    axios
      .post("orders.json", order)
      .then((response) => {
        setLoading(false);
        navigate("/");
      })
      .catch((error) => {
        console.log("error occure", error);
        setLoading(false);
      });
  };
  let form = (
    <form>
      <input
        className={classes.Input}
        type="text"
        name="name"
        placeholder="You Name"
      />
      <input
        className={classes.Input}
        type="email"
        name="email"
        placeholder="Your Mail"
      />
      <input
        className={classes.Input}
        type="text"
        name="street"
        placeholder="Street"
      />
      <input
        className={classes.Input}
        type="text"
        name="postal"
        placeholder="Postal Code"
      />
      <Button btnType="Success" clicked={orderHandler}>
        ORDER
      </Button>
    </form>
  );
  if (loading == true) {
    form = <Spinner />;
  }
  return (
    <div className={classes.ContactData}>
      <h4>Enter your Contact Data</h4>
      {form}
    </div>
  );
}
