import { useEffect, useState } from "react";
import classes from "./Checkout.module.css";
import { Outlet, useLocation, useNavigate } from "react-router-dom";
import CheckoutSummary from "../../Components/Order/CheckoutSummary/CheckoutSummary";
import Spinner from "../../Components/UI/Spinner/Spinner";
const Checkout = () => {
  const [ingredients, setIngredients] = useState(null);
  const [totalPrice, setTotalPrice] = useState(0);
  const [loading, setLoading] = useState(true);
  const navigate = useNavigate();
  const location = useLocation();
  useEffect(() => {
    const query = new URLSearchParams(location.search);
    const currIngredients = {};
    let price = 0;
    for (let param of query.entries()) {
      if (param[0] == "price") {
        price = +param[1];
      } else {
        currIngredients[param[0]] = +param[1];
      }
    }
    setIngredients(currIngredients);
    setTotalPrice(price);
    setLoading(false);
  }, [location.search]);

  const checkoutCancelledHandler = () => {
    navigate(-1);
  };

  const checkoutContinuedHandler = () => {
    navigate("/checkout/contact-data");
  };

  let checkoutSummary = (
    <CheckoutSummary
      checkoutContinued={checkoutContinuedHandler}
      checkoutCancelled={checkoutCancelledHandler}
      ingredients={ingredients}
    />
  );

  if (loading == true) {
    checkoutSummary = <Spinner />;
  }

  return (
    <div className={classes.Checkout}>
      {checkoutSummary}
      <Outlet context={[ingredients]} />
    </div>
  );
};

export default Checkout;
