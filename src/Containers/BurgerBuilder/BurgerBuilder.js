<<<<<<< Updated upstream
import React, { useEffect, useState } from "react";

=======
import React, { Component } from "react";
>>>>>>> Stashed changes
import Auxi from "../../hoc/_Aux/Auxi";
import Burger from "../../Components/Burger/Burger";
import BuildControls from "../../Components/Burger/BuildControls/BuildControls";
import Modal from "../../Components/UI/Modal/Modal";
import OrderSummary from "../../Components/Burger/OrderSummary/OrderSummary";
<<<<<<< Updated upstream
import axios from "../../axios-order";
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";
import Spinner from "../../Components/UI/Spinner/Spinner";
import { useNavigate } from "react-router-dom";
=======
import axios from "../../axios-orders";

>>>>>>> Stashed changes
const INGREDIENT_PRICES = {
  salad: 0.5,
  cheese: 0.4,
  meat: 1.3,
  bacon: 0.7,
};

<<<<<<< Updated upstream
const BurgerBuilder = () => {
  const navigate = useNavigate();
  const [ingredients, setIngredients] = useState(null);
  const [totalPrice, setTotalPrice] = useState(4);
  const [purchasable, setPurchasable] = useState(false);
  const [purchasing, setPurchasing] = useState(false);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    axios
      .get("/ingredients.json")
      .then((response) => {
        setIngredients(response.data);
        let price = Object.keys(ingredients)
          .map((igKey) => {
            return ingredients[igKey];
          })
          .reduce((el, sum) => {
            return el + sum;
          }, 0);
        setTotalPrice(totalPrice + price);
      })
      .catch((error) => {
        setError(error);
      });
  }, []);


  const updatePurchaseState = (ingredients) => {
=======
class BurgerBuilder extends Component {
  // constructor(props) {
  //   super(props);
  // }

  state = {
    ingredients: {
      salad: 0,
      bacon: 0,
      cheese: 0,
      meat: 0,
    },
    totalPrice: 4,
    purchasable: false,
    purchasing: false,
  };

  ingredientAddedHandler = (type) => {
    const oldCount = this.state.ingredients[type];
    const newCount = oldCount + 1;
    const updatedIngredients = {
      ...this.state.ingredients,
    };
    updatedIngredients[type] = newCount;
    const updatedPrice = this.state.totalPrice + INGREDIENT_PRICES[type];
    this.setState({
      ingredients: updatedIngredients,
      totalPrice: updatedPrice,
      purchasable: true,
    });
    this.updatedPurchaseState(updatedIngredients);
  };

  ingredientRemovedHandler = (type) => {
    const oldCount = this.state.ingredients[type];
    const newCount = oldCount - 1;
    if (newCount < 0) {
      this.setState({ purchasable: false });
      return;
    }

    const updatedIngredients = {
      ...this.state.ingredients,
    };
    updatedIngredients[type] = newCount;
    const updatedPrice = this.state.totalPrice - INGREDIENT_PRICES[type];
    this.setState({
      ingredients: updatedIngredients,
      totalPrice: updatedPrice,
    });
    this.updatedPurchaseState(updatedIngredients);
  };

  updatedPurchaseState(ingredients) {
>>>>>>> Stashed changes
    const sum = Object.keys(ingredients)
      .map((igKey) => {
        return ingredients[igKey];
      })
      .reduce((sum, el) => {
        return sum + el;
      }, 0);
<<<<<<< Updated upstream
    setPurchasable(sum > 0 ? true : false);
  };

  const addIngredientHandler = (type) => {
    const oldCount = ingredients[type];
    const updatedCount = oldCount + 1;
    const updatedIngredients = {
      ...ingredients,
    };
    updatedIngredients[type] = updatedCount;
    const priceAddition = INGREDIENT_PRICES[type];
    const oldPrice = totalPrice;
    const newPrice = oldPrice + priceAddition;
    setTotalPrice(newPrice);
    setIngredients(updatedIngredients);
    updatePurchaseState(updatedIngredients);
  };

  const removeIngredientHandler = (type) => {
    const oldCount = ingredients[type];
    if (oldCount <= 0) {
      return;
    }
    const updatedCount = oldCount - 1;
    const updatedIngredients = {
      ...ingredients,
    };
    updatedIngredients[type] = updatedCount;
    const priceDeduction = INGREDIENT_PRICES[type];
    const oldPrice = totalPrice;
    const newPrice = oldPrice - priceDeduction;
    setTotalPrice(newPrice);
    setIngredients(updatedIngredients);
    updatePurchaseState(updatedIngredients);
  };

  const purchaseHandler = () => {
    setPurchasing(true);
  };

  const purchaseCancelHandler = () => {
    setPurchasing(false);
  };

  const purchaseContinueHandler = () => {
    const queryParams = [];
    for (let i in ingredients) {
      queryParams.push(
        encodeURIComponent(i) + "=" + encodeURIComponent(ingredients[i])
      );
    }
    const queryString = queryParams.join("&");

    navigate({
      pathname: "/checkout",
      search: "?" + queryString,
    });
  };

  const disabledInfo = {
    ...ingredients,
  };
  for (let key in disabledInfo) {
    disabledInfo[key] = disabledInfo[key] <= 0;
  }
  // {salad: true, meat: false, ...}
  let orderSummary = null;
  let burger = error ? <p>Ingredients can't be loaded!</p> : <Spinner />;
  if (ingredients) {
    burger = (
      <Auxi>
        <Burger ingredients={ingredients} />
        <BuildControls
          ingredientAdded={addIngredientHandler}
          ingredientRemoved={removeIngredientHandler}
          disabled={disabledInfo}
          purchasable={purchasable}
          ordered={purchaseHandler}
          price={totalPrice}
        />
      </Auxi>
    );
    orderSummary = (
      <OrderSummary
        ingredients={ingredients}
        price={totalPrice}
        purchaseCancelled={purchaseCancelHandler}
        purchaseContinued={purchaseContinueHandler}
      />
    );
    if (loading) {
      orderSummary = <Spinner />;
    }
  }

  return (
    <Auxi>
      <Modal show={purchasing} modalClosed={purchaseCancelHandler}>
        {orderSummary}
      </Modal>
      {burger}
    </Auxi>
  );
};

export default withErrorHandler(BurgerBuilder, axios);
=======
    this.setState({ purchasable: sum > 0 });
  }

  purchaseHandler = () => {
    this.setState({ purchasing: true });
  };

  purchaseCancelHandler = () => {
    console.log("purchaseCancelHandler");
    this.setState({ purchasing: false });
  };

  purchaseContinueHandler = () => {
    const order = {
      ingredients: this.state.ingredients,
      price: this.state.price,
      customer: {
        name: "Ahmad Zaza",
        address: {
          street: "AL-Kaser",
          zipCode: "123",
          country: "syria",
        },
        email: "ahmadzazaz98@gmail.com",
      },
      deliveryMethod: "fastest",
    };

    axios
      .post("/orders.json", order)
      .then((response) => console.log(response))
      .catch((error) => console.log(error));

  };

  render() {
    return (
      <Auxi>
        <Modal
          show={this.state.purchasing}
          modalClosed={this.purchaseCancelHandler}
        >
          <OrderSummary
            purchasing={this.state.purchasing}
            ingredients={this.state.ingredients}
            purchaceCanceled={this.purchaseCancelHandler}
            purchaceContinued={this.purchaseContinueHandler}
          ></OrderSummary>
        </Modal>
        <Burger ingredients={this.state.ingredients}></Burger>
        <BuildControls
          price={this.state.totalPrice}
          ingredientAdded={this.ingredientAddedHandler}
          ingredientRemoved={this.ingredientRemovedHandler}
          ordered={this.purchaseHandler}
          purchasable={this.state.purchasable}
        />
      </Auxi>
    );
  }
}

export default BurgerBuilder;
>>>>>>> Stashed changes
