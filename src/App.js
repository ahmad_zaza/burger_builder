<<<<<<< Updated upstream
import React from "react";
import Layout from "./hoc/Layout/Layout";
import BurgerBuilder from "./Containers/BurgerBuilder/BurgerBuilder";
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";
import Order from "./Containers/Order/Order";
import Checkout from "./Containers/Checkout/Checkout";
import ContactData from "./Containers/Checkout/ContactData/ContactData";
const App = (props) => {
  const router = createBrowserRouter(
    createRoutesFromElements(
      <Route>
        <Route index element={<BurgerBuilder />} />
        <Route path="checkout" element={<Checkout />}>
          <Route path="contact-data" element={<ContactData />} />
        </Route>
        <Route path="orders" element={<Order />} />
      </Route>
    )
  );
  return (
    <>
      <RouterProvider router={router} />
    </>
  );
};

export default App;
=======
import React, { Component } from "react";
import Radium from "radium";
import BurgerBuilder from "./Containers/BurgerBuilder/BurgerBuilder";
import Layout from "./hoc/Layout/Layout";

class App extends Component {
  render() {
    return (
      <div>
        <Layout>
          <BurgerBuilder />
        </Layout>
      </div>
    );
  }
}

export default Radium(App);
>>>>>>> Stashed changes
